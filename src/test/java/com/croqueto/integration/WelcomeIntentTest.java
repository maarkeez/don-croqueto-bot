package com.croqueto.integration;

import com.google.cloud.dialogflow.v2.QueryResult;
import org.junit.jupiter.api.Test;

import static com.croqueto.integration.IntegrationTestFactory.croquetoApi;
import static com.croqueto.integration.IntegrationTestFactory.firstMessageOf;
import static com.croqueto.integration.IntegrationTestFactory.newSessionId;
import static org.assertj.core.api.Assertions.assertThat;

class WelcomeIntentTest {

    @Test
    void welcomeIntentWhenSayHi() {

        // Given
        final String sessionId = newSessionId();
        final String userSays = "hola";

        // When
        final QueryResult queryResult = croquetoApi().intent().findIntentBySessionIdAndText(sessionId, userSays);

        // Then
        assertThat(queryResult.getAction()).isEqualTo("input.welcome");
        assertThat(firstMessageOf(queryResult))
                .isEqualTo("¡Hola! Soy Don Croqueto, tu asistente personal de recetas. ¿Te apetece cocinar algo hoy o quieres crear una nueva receta?");

    }

}
