package com.croqueto.recipe;

import static java.util.Objects.isNull;
import static java.util.stream.Collectors.joining;
import static lombok.AccessLevel.PRIVATE;

import java.util.List;
import lombok.NoArgsConstructor;
import lombok.Singular;
import lombok.Value;
import lombok.experimental.SuperBuilder;

@Value
@SuperBuilder(toBuilder = true)
@NoArgsConstructor(force = true, access = PRIVATE)
public class Recipe {

  String name;

  @Singular
  List<Ingredient> ingredients;

  String steps;

  @Value
  @SuperBuilder(toBuilder = true)
  @NoArgsConstructor(force = true, access = PRIVATE)
  public static class Ingredient {
    UnitWeight unitWeight;
    String name;

    public String displayIngredient() {
      return isNull(unitWeight)
          ? name
          : "%f %s de %s".formatted(unitWeight.getAmount(), unitWeight.getUnit(), name);
    }

  }

  @Value
  @SuperBuilder(toBuilder = true)
  @NoArgsConstructor(force = true, access = PRIVATE)
  public static class UnitWeight {
    double amount;
    String unit;
  }

  public String displayIngredients() {
    return ingredients.stream()
        .map(Ingredient::displayIngredient)
        .collect(joining(", "));
  }
}
