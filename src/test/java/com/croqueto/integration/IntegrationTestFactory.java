package com.croqueto.integration;

import static lombok.AccessLevel.PRIVATE;

import com.croqueto.CroquetoApi;
import com.croqueto.CroquetoApiModule;
import com.croqueto.recipe.GitLabCredentials;
import com.croqueto.recipe.Recipe.Ingredient;
import com.croqueto.recipe.RecipeConfigData;
import com.croqueto.recipe.RecipeModule;
import com.croqueto.recipe.RecipeService;
import com.google.cloud.dialogflow.v2.QueryResult;
import java.net.URL;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Predicate;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;

@NoArgsConstructor(access = PRIVATE)
public class IntegrationTestFactory {

  private static final String DIALOG_FLOW_PROJECT_ID = getenv("GOOGLE_PROJECT_ID");
  private static final CroquetoApi CROQUETO_API = CroquetoApiModule.croquetoApi(DIALOG_FLOW_PROJECT_ID);
  private static final String GITLAB_TOKEN = getenv("GITLAB_TOKEN");
  private static final String RECIPES_FILE_BRANCH = getenv("RECIPES_FILE_BRANCH");

  public static final URL BACKEND_APP_BASE_URL = url(getenv("BACKEND_APP_BASE_URL"));

  public static CroquetoApi croquetoApi() {
    return CROQUETO_API;
  }

  public static String newSessionId() {
    return UUID.randomUUID()
        .toString();
  }

  public static String dialogFlowProjectId() {
    return DIALOG_FLOW_PROJECT_ID;
  }

  @SneakyThrows
  public static URL url(String url) {
    return new URL(url);
  }

  static String firstMessageOf(QueryResult summaryResult) {
    return summaryResult.getFulfillmentMessagesList()
        .get(0)
        .getText()
        .getText(0);
  }

  private static String getenv(String key) {
    return Optional.ofNullable(System.getenv(key))
        .filter(Predicate.not(String::isEmpty))
        .orElseThrow(() -> new RuntimeException("Missing environment variable: %s".formatted(key)));
  }

  static RecipeService recipeService() {
    return RecipeModule.recipeModule().recipeService(gitlabCredentials(), recipeConfigData());
  }

  private static GitLabCredentials gitlabCredentials() {
    return GitLabCredentials.builder()
        .baseUrl(url("https://gitlab.com"))
        .accessToken(GITLAB_TOKEN)
        .build();
  }

  private static RecipeConfigData recipeConfigData() {
    return RecipeConfigData.builder()
        .projectNamespace("maarkeez")
        .projectName("don-croqueto-bot-recipes")
        .fileRef(RECIPES_FILE_BRANCH)
        .filePath("recipes.json")
        .build();
  }

  static Ingredient ingredient(String name) {
    return Ingredient.builder()
        .name(name)
        .build();
  }


}
