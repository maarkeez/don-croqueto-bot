package com.croqueto.backend.api.chat;

import static com.croqueto.backend.api.chat.ChatModule.chatModule;
import static org.assertj.core.api.Assertions.assertThat;

import com.croqueto.recipe.Recipe;
import java.util.UUID;
import org.junit.jupiter.api.Test;

class ChatContextServiceTest {

  private final ChatContextService chatContextService = chatModule().chatContextService();

  @Test
  void testInsertAndFind() {

    // Given:
    final String sessionId = UUID.randomUUID().toString();
    final Recipe recipeInserted = Recipe.builder()
        .name(UUID.randomUUID().toString())
        .build();

    chatContextService.insert(sessionId, recipeInserted);

    // When:
    final Recipe recipeFound = chatContextService.findBySessionId(sessionId);

    // Then
    assertThat(recipeFound).isEqualTo(recipeInserted);

  }

}