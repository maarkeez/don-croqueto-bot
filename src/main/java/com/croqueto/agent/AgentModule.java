package com.croqueto.agent;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import static lombok.AccessLevel.PRIVATE;

@NoArgsConstructor(access = PRIVATE)
public class AgentModule {

    private static final AgentModule INSTANCE = new AgentModule();

    public static AgentModule agentModule() {
        return INSTANCE;
    }

    public AgentApi agentApi(@NonNull String projectId) {
        return new AgentApiImpl(projectId, new ObjectMapper());
    }

}
