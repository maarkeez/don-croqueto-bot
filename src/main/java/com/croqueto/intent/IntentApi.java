package com.croqueto.intent;

import com.google.cloud.dialogflow.v2.QueryResult;

public interface IntentApi {
    QueryResult findIntentBySessionIdAndText(String sessionId, String text);
}
