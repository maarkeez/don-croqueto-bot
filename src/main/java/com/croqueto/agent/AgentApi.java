package com.croqueto.agent;

import lombok.NonNull;

import java.net.URL;
import java.nio.file.Path;

public interface AgentApi {

    void changeWebhookUrl(@NonNull URL newWebhookUrl, @NonNull Path agentFilePath);

    void restoreAgent(@NonNull Path zipFile);
}
