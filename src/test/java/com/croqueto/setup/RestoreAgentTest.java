package com.croqueto.setup;

import java.net.URI;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;

import static com.croqueto.integration.IntegrationTestFactory.croquetoApi;
import static java.lang.System.getenv;

@Slf4j
class RestoreAgentTest {

    private final Path agentCodeFolder = Paths.get("don-croqueto");
    private final Path agentFilePath = agentCodeFolder.resolve("agent.json");

    @Test
    @SneakyThrows
    void deployAgent() {

        final URL newAgentUrl = new URI(getenv("BACKEND_APP_BASE_URL"))
            .resolve("/croqueto/api/event")
            .toURL();
        
        croquetoApi().agent().changeWebhookUrl(newAgentUrl, agentFilePath);
        final Path agentZipFile = croquetoApi().zip().zipDirectory(agentCodeFolder);
        croquetoApi().agent().restoreAgent(agentZipFile);

    }

}
