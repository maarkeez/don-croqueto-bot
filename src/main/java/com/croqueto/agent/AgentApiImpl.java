package com.croqueto.agent;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.cloud.dialogflow.v2.AgentsClient;
import com.google.cloud.dialogflow.v2.RestoreAgentRequest;
import com.google.protobuf.ByteString;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;

import java.net.URL;
import java.nio.file.Path;

import static com.google.protobuf.ByteString.copyFrom;
import static java.nio.file.Files.readAllBytes;

@RequiredArgsConstructor
class AgentApiImpl implements AgentApi {

    @NonNull
    private final String projectId;
    @NonNull
    private final ObjectMapper objectMapper;

    @Override
    @SneakyThrows
    public void changeWebhookUrl(@NonNull URL newWebhookUrl, @NonNull Path agentFilePath) {
        final JsonNode agent = objectMapper.readValue(agentFilePath.toFile(), JsonNode.class);
        final ObjectNode webhook = (ObjectNode) agent.get("webhook");
        webhook.put("url", newWebhookUrl.toString());
        objectMapper.writeValue(agentFilePath.toFile(), agent);
    }

    @Override
    @SneakyThrows
    public void restoreAgent(@NonNull Path agentZipFile) {

        try (final AgentsClient agentsClient = AgentsClient.create()) {

            final String projectIdPath = "projects/%s".formatted(projectId);
            final byte[] text = readAllBytes(agentZipFile);
            final ByteString zipCompressedRawBytes = copyFrom(text);

            final RestoreAgentRequest restoreRequest = RestoreAgentRequest.getDefaultInstance()
                    .toBuilder()
                    .setParent(projectIdPath)
                    .setAgentContent(zipCompressedRawBytes)
                    .build();

            agentsClient.restoreAgentAsync(restoreRequest).get();

            agentsClient.trainAgentAsync(projectIdPath).get();

        }

    }
}
