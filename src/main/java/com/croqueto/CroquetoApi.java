package com.croqueto;

import com.croqueto.agent.AgentApi;
import com.croqueto.intent.IntentApi;
import com.croqueto.zip.ZipApi;

public interface CroquetoApi {
    IntentApi intent();

    AgentApi agent();

    ZipApi zip();
}
