package com.croqueto.backend.api;

import static java.util.Optional.ofNullable;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.ResponseEntity.ok;

import com.croqueto.backend.api.chat.ChatContextService;
import com.croqueto.backend.api.logger.Logger;
import com.croqueto.backend.api.model.FulfillmentMessage;
import com.croqueto.backend.api.model.FulfillmentMessage.Text;
import com.croqueto.backend.api.model.WebhookRequest;
import com.croqueto.backend.api.model.WebhookResponse;
import com.croqueto.recipe.Recipe;
import com.croqueto.recipe.Recipe.Ingredient;
import com.croqueto.recipe.Recipe.UnitWeight;
import com.croqueto.recipe.RecipeService;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;


@RestController
@RequiredArgsConstructor(onConstructor = @__({@Autowired}))
public class CroquetoApiController implements CroquetoApi {

  private final Logger log;
  private final ChatContextService chatContextService;
  private final RecipeService recipeService;

  @Override
  public ResponseEntity<Map<String, Recipe>> eventFindAll() {
    return ok().body(chatContextService.findAll());
  }

  @Override
  public ResponseEntity<List<Recipe>> recipeFindAll() {
    return ok().body(recipeService.findAll());
  }

  @Override
  public ResponseEntity<WebhookResponse> agentEvent(WebhookRequest webhookRequest) {

    log.request(webhookRequest);

    final String action = collectActionOrFail(webhookRequest);

    final WebhookResponse webhookResponse = switch (action) {
      case "input.recipe.create.ingredient" -> handleAddIngredient(webhookRequest);
      case "input.recipe.create.summary" -> handleCreateRecipeSummary(webhookRequest);
      case "input.recipe.create.summary.confirmation" -> handleSummaryConfirmation(webhookRequest);
      case "input.recipe.read" -> handleRecipeRead(webhookRequest);
      default -> throw new HttpClientErrorException(BAD_REQUEST, "Invalid query_result.action: " + action);
    };

    log.response(webhookResponse);
    return ok().body(webhookResponse);

  }

  private WebhookResponse handleAddIngredient(WebhookRequest webhookRequest) {

    final String sessionId = collectSessionIdOrFail(webhookRequest);
    final Optional<UnitWeight> ingredientWeight = collectWeight(webhookRequest);
    final String ingredientName = collectParameterOrFail(webhookRequest, "ingredient");
    final Ingredient ingredient = ingredientWeight.isPresent()
        ? Ingredient.builder().name(ingredientName).unitWeight(ingredientWeight.get()).build()
        : Ingredient.builder().name(ingredientName).build();

    final Recipe chatContext = chatContextService.findBySessionId(sessionId).toBuilder()
        .ingredient(ingredient)
        .build();

    chatContextService.insert(sessionId, chatContext);

    return buildWebhookResponse("Anotado, ¿algo más?");

  }

  private WebhookResponse handleCreateRecipeSummary(WebhookRequest webhookRequest) {

    final String sessionId = collectSessionIdOrFail(webhookRequest);

    final Recipe recipe = chatContextService.findBySessionId(sessionId).toBuilder()
        .steps(collectParameterOrFail(webhookRequest, "steps"))
        .name(collectParameterOrFail(webhookRequest, "recipe"))
        .build();

    chatContextService.insert(sessionId, recipe);

    final String recipeSummary = """
        ¡Genial! Pues esta es la receta que se va a guardar.
                        
        - Receta: %s
        - Ingredientes: %s
        - Pasos a seguir: %s
                        
        ¿Está todo correcto?
                        
        """.formatted(recipe.getName(), recipe.displayIngredients(), recipe.getSteps());

    return buildWebhookResponse(recipeSummary);
  }

  private WebhookResponse handleSummaryConfirmation(WebhookRequest webhookRequest) {

    final String sessionId = collectSessionIdOrFail(webhookRequest);

    final Recipe recipe = chatContextService.findBySessionId(sessionId);
    chatContextService.delete(sessionId);
    recipeService.insert(recipe);

    return buildWebhookResponse("Vale, me guardo la receta para que puedas consultarla en el futuro. ¡Adiós!");
  }

  private WebhookResponse handleRecipeRead(WebhookRequest webhookRequest) {

    final String recipeName = collectParameterOrFail(webhookRequest, "recipe");
    final Optional<Recipe> recipeOpt = recipeService.findAll().stream()
        .filter(recipe -> recipeName.equalsIgnoreCase(recipe.getName()))
        .findFirst();

    return recipeOpt.isPresent()
        ? buildWebhookResponse(buildRecipeReadMessage(recipeOpt.get()))
        : buildWebhookResponse("Lo siento, pero yo tampoco sé cocinar %s".formatted(recipeName));
  }

  private String buildRecipeReadMessage(Recipe recipe) {
    return """
        Aquí tienes la receta
                        
        - Receta: %s
        - Ingredientes: %s
        - Pasos a seguir: %s
                        
        ¡Qué aproveche!""".formatted(recipe.getName(), recipe.displayIngredients(), recipe.getSteps());
  }

  private String collectActionOrFail(WebhookRequest webhookRequest) {
    return ofNullable(webhookRequest)
        .map(WebhookRequest::getQueryResult)
        .map(WebhookRequest.QueryResult::getAction)
        .orElseThrow(() -> new HttpClientErrorException(BAD_REQUEST, "Missing query_result.action field"));
  }

  private String collectSessionIdOrFail(WebhookRequest webhookRequest) {
    return ofNullable(webhookRequest)
        .map(WebhookRequest::getSession)
        .orElseThrow(() -> new HttpClientErrorException(BAD_REQUEST, "Missing session_id field"));
  }

  @SuppressWarnings("unchecked")
  private <T> T collectParameterOrFail(WebhookRequest webhookRequest, String parameterKey) {

    return ofNullable(webhookRequest)
        .map(WebhookRequest::getQueryResult)
        .map(WebhookRequest.QueryResult::getParameters)
        .map(params -> (T) params.get(parameterKey))
        .orElseThrow(() -> new HttpClientErrorException(BAD_REQUEST, "Missing parameter with key: " + parameterKey));
  }

  @SuppressWarnings("unchecked")
  private Optional<UnitWeight> collectWeight(WebhookRequest webhookRequest) {

    return ofNullable(webhookRequest)
        .map(WebhookRequest::getQueryResult)
        .map(WebhookRequest.QueryResult::getParameters)
        .map(params -> params.get("weight"))
        .filter(weightParameter -> weightParameter.getClass().isAssignableFrom(Map.class))
        .map(weightObj -> (Map<String, Object>) weightObj)
        .map(weightMap ->
            UnitWeight.builder()
                .amount((double) weightMap.get("amount"))
                .unit((String) weightMap.get("unit"))
                .build()
        );

  }

  private WebhookResponse buildWebhookResponse(String response) {

    final Text text = Text.builder()
        .text(response)
        .build();

    final FulfillmentMessage fulfillmentMessage = FulfillmentMessage.builder()
        .text(text)
        .build();

    return WebhookResponse.builder()
        .fulfillmentMessage(fulfillmentMessage)
        .build();

  }

}
