package com.croqueto.integration;

import static com.croqueto.integration.IntegrationTestFactory.croquetoApi;
import static com.croqueto.integration.IntegrationTestFactory.firstMessageOf;
import static com.croqueto.integration.IntegrationTestFactory.newSessionId;
import static org.assertj.core.api.Assertions.assertThat;

import com.google.cloud.dialogflow.v2.QueryResult;
import org.junit.jupiter.api.Test;

class GoodbyeIntentTest {

  @Test
  void welcomeIntentWhenSayHi() {

    // Given
    final String sessionId = newSessionId();
    final String userSays = "¡Adiós!";

    // When
    final QueryResult queryResult = croquetoApi().intent().findIntentBySessionIdAndText(sessionId, userSays);

    // Then
    assertThat(queryResult.getAction()).isEqualTo("input.goodbye");
    assertThat(firstMessageOf(queryResult)).isIn(
        "Adiós",
        "¡Hasta pronto!",
        "Ha sido un placer hablar contigo, ¡nos vemos pronto!"
    );

  }

}
