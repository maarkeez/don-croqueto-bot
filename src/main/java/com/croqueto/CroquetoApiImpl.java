package com.croqueto;

import com.croqueto.agent.AgentApi;
import com.croqueto.intent.IntentApi;
import com.croqueto.zip.ZipApi;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
class CroquetoApiImpl implements CroquetoApi {

    @NonNull
    private final IntentApi intentApi;
    @NonNull
    private final AgentApi agentApi;
    @NonNull
    private final ZipApi zipApi;

    @Override
    public IntentApi intent() {
        return intentApi;
    }

    @Override
    public AgentApi agent() {
        return agentApi;
    }

    @Override
    public ZipApi zip() {
        return zipApi;
    }
}
