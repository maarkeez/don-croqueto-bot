package com.croqueto.backend.api.model;

import static lombok.AccessLevel.PRIVATE;

import java.util.List;
import lombok.NoArgsConstructor;
import lombok.Singular;
import lombok.Value;
import lombok.experimental.SuperBuilder;

@Value
@SuperBuilder(toBuilder = true)
@NoArgsConstructor(force = true, access = PRIVATE)
public class FulfillmentMessage {
  Text text;

  @Value
  @SuperBuilder(toBuilder = true)
  @NoArgsConstructor(force = true, access = PRIVATE)
  public static class Text {
    @Singular("text")
    List<String> text;
  }

}

