package com.croqueto.backend.api.logger;

import com.croqueto.backend.api.model.WebhookRequest;
import com.croqueto.backend.api.model.WebhookResponse;

public interface Logger {
  void request(WebhookRequest webhookRequest);

  void response(WebhookResponse webhookResponse);
}
