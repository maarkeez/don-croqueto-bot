package com.croqueto.integration;

import static com.croqueto.integration.IntegrationTestFactory.croquetoApi;
import static com.croqueto.integration.IntegrationTestFactory.firstMessageOf;
import static com.croqueto.integration.IntegrationTestFactory.ingredient;
import static com.croqueto.integration.IntegrationTestFactory.newSessionId;
import static com.croqueto.integration.IntegrationTestFactory.recipeService;
import static org.assertj.core.api.Assertions.assertThat;

import com.croqueto.recipe.Recipe;
import com.google.cloud.dialogflow.v2.QueryResult;
import org.junit.jupiter.api.Test;

class RecipeReadTest extends BaseIntegrationTest {

  @Test
  void readRecipeWhenExisting() {

    // Given: an existing recipe
    final String sessionId = newSessionId();
    final Recipe recipe = Recipe.builder()
        .name("cocido")
        .ingredient(ingredient("garbanzos"))
        .ingredient(ingredient("sal"))
        .steps("Echar todos los ingredientes en una olla a presión, llenar de agua hasta que cubra y dejar cociendo 20 minutos.")
        .build();
    recipeService().insert(recipe);

    // When: asking to cook that recipe
    final QueryResult greetingResult = croquetoApi().intent().findIntentBySessionIdAndText(sessionId, "hola");
    final QueryResult recipeReadResult = croquetoApi().intent().findIntentBySessionIdAndText(sessionId, "quiero cocinar %s".formatted(recipe.getName()));

    // Then: the agent should send us the recipe through the conversation
    assertThat(greetingResult.getAction()).isEqualTo("input.welcome");
    assertThat(recipeReadResult.getAction()).isEqualTo("input.recipe.read");
    assertThat(firstMessageOf(recipeReadResult))
        .contains(recipe.getName())
        .contains(recipe.getSteps())
        .contains(recipe.displayIngredients());

  }

}
