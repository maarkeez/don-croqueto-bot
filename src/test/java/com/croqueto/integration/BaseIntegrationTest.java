package com.croqueto.integration;

import static com.croqueto.integration.IntegrationTestFactory.BACKEND_APP_BASE_URL;
import static com.croqueto.integration.IntegrationTestFactory.recipeService;
import static com.croqueto.integration.backend.api.ActuatorApi.Status.UP;
import static com.croqueto.integration.backend.api.BackendApi.backendApi;
import static java.util.concurrent.TimeUnit.MINUTES;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.awaitility.Awaitility.await;

import com.croqueto.integration.backend.api.ActuatorApi;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;

@Slf4j
public class BaseIntegrationTest {

  private final ActuatorApi actuatorApi = backendApi().actuator(BACKEND_APP_BASE_URL);

  @BeforeEach
  public void setup() {

    await().atMost(2, MINUTES)
        .pollInterval(3, SECONDS)
        .until(this::backendApiIsUp);

    recipeService().deleteAll();

  }

  private boolean backendApiIsUp() {

    try {
      return UP.equals(actuatorApi.health().getStatus());
    } catch (Exception exception) {
      log.warn("Could not get backend status. Cause: {}", exception.getMessage());
      return false;
    }

  }
}
