package com.croqueto.integration;

import com.google.cloud.dialogflow.v2.QueryResult;
import org.junit.jupiter.api.Test;

import static com.croqueto.integration.IntegrationTestFactory.croquetoApi;
import static com.croqueto.integration.IntegrationTestFactory.firstMessageOf;
import static com.croqueto.integration.IntegrationTestFactory.newSessionId;
import static java.util.List.of;
import static org.assertj.core.api.Assertions.assertThat;

class DefaultIntentTest {

    @Test
    void defaultIntentWhenSomethingUnknown() {

        // Given
        final String sessionId = newSessionId();
        final String userSays = "asdfg";

        // When
        final QueryResult queryResult = croquetoApi().intent().findIntentBySessionIdAndText(sessionId, userSays);

        // Then
        assertThat(queryResult.getAction()).isEqualTo("input.unknown");
        assertThat(of(
                "Ups, no he entendido a que te refieres.",
                "¿Podrías repetirlo, por favor?",
                "¿Disculpa?",
                "¿Decías?",
                "¿Cómo?",
                "Lo siento, no te he entendido. ¿Podrías repetírmelo?"
        )).contains(firstMessageOf(queryResult));

    }

}
