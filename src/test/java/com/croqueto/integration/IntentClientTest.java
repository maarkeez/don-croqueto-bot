package com.croqueto.integration;

import com.google.cloud.dialogflow.v2.AgentName;
import com.google.cloud.dialogflow.v2.IntentsClient;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import static com.croqueto.integration.IntegrationTestFactory.dialogFlowProjectId;
import static com.google.cloud.dialogflow.v2.AgentName.of;
import static com.google.cloud.dialogflow.v2.IntentsClient.create;
import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
class IntentClientTest {


    @SneakyThrows
    @Test
    void hasOneIntent() {

        try (final IntentsClient intentsClient = create()) {
            final AgentName dialogFlowProject = of(dialogFlowProjectId());
            final boolean hasOneIntentAtLeast = intentsClient.listIntents(dialogFlowProject).iterateAll().iterator().hasNext();
            assertThat(hasOneIntentAtLeast).isTrue();
        }

    }

}
