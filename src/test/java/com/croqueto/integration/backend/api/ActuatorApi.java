package com.croqueto.integration.backend.api;

import feign.RequestLine;
import lombok.NoArgsConstructor;
import lombok.Value;
import lombok.experimental.SuperBuilder;

public interface ActuatorApi {
  
  @RequestLine( "GET /actuator/health" )
  Health health ();
  
  
  @Value
  @SuperBuilder( toBuilder = true )
  @NoArgsConstructor( force = true )
  class Health {
    Status status;
  }
  
  enum Status {
    DOWN,
    OUT_OF_SERVICE,
    UP,
    UNKNOWN
  }
  
}
