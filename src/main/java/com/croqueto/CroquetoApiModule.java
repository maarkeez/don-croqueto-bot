package com.croqueto;

import com.croqueto.agent.AgentApi;
import com.croqueto.intent.IntentApi;
import com.croqueto.zip.ZipApi;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import static com.croqueto.agent.AgentModule.agentModule;
import static com.croqueto.intent.IntentModule.intentModule;
import static com.croqueto.zip.ZipModule.zipModule;
import static lombok.AccessLevel.PRIVATE;

@NoArgsConstructor(access = PRIVATE)
public class CroquetoApiModule {

    public static CroquetoApi croquetoApi(@NonNull String projectId) {

        final IntentApi intentApi = intentModule().intentApi(projectId);
        final AgentApi agentApi = agentModule().agentApi(projectId);
        final ZipApi zipApi = zipModule().zipApi();

        return new CroquetoApiImpl(intentApi, agentApi, zipApi);
    }

}
