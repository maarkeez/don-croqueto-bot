package com.croqueto.zip;

import lombok.NoArgsConstructor;

import static lombok.AccessLevel.PRIVATE;

@NoArgsConstructor(access = PRIVATE)
public class ZipModule {

    private static final ZipModule INSTANCE = new ZipModule();

    public static ZipModule zipModule() {
        return INSTANCE;
    }

    public ZipApi zipApi() {
        return ZipApiImpl.getInstance();
    }

}
