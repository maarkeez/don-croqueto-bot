package com.croqueto.recipe;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.Synchronized;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.models.Project;
import org.gitlab4j.api.models.RepositoryFile;

@RequiredArgsConstructor
class RecipeServiceImpl implements RecipeService {

  @NonNull
  private final ObjectMapper objectMapper;
  @NonNull
  private final GitLabApi gitLabApi;
  @NonNull
  private final RecipeConfigData recipeConfigData;

  @Override
  @SneakyThrows
  @Synchronized
  public void insert(Recipe recipe) {

    final List<Recipe> recipes = findAll();
    recipes.add(recipe);

    final Project project = findProject();
    final RepositoryFile actualRecipes = gitLabApi.getRepositoryFileApi().getFile(project, recipeConfigData.getFilePath(), recipeConfigData.getFileRef());
    actualRecipes.encodeAndSetContent(objectMapper.writeValueAsString(recipes));

    gitLabApi.getRepositoryFileApi().updateFile(
        project,
        actualRecipes,
        recipeConfigData.getFileRef(),
        "Added new recipe: %s".formatted(recipe.getName())
    );

  }

  @Override
  @SneakyThrows
  @Synchronized
  public List<Recipe> findAll() {

    final RepositoryFile recipes = gitLabApi.getRepositoryFileApi().getFile(findProject(), recipeConfigData.getFilePath(), recipeConfigData.getFileRef());
    final String recipesJsonStr = recipes.getDecodedContentAsString();

    return objectMapper.readValue(recipesJsonStr, new TypeReference<>() {
    });

  }

  @Override
  @SneakyThrows
  public void deleteAll() {

    final Project project = findProject();
    final RepositoryFile actualRecipes = gitLabApi.getRepositoryFileApi().getFile(project, recipeConfigData.getFilePath(), recipeConfigData.getFileRef());
    actualRecipes.encodeAndSetContent("[]");

    gitLabApi.getRepositoryFileApi().updateFile(
        project,
        actualRecipes,
        recipeConfigData.getFileRef(),
        "Deleted all recipes"
    );

  }

  @SneakyThrows
  private Project findProject() {
    final String projectPathWithNamespace = "%s/%s".formatted(recipeConfigData.getProjectNamespace(), recipeConfigData.getProjectName());
    return gitLabApi.getProjectApi().getOwnedProjectsStream()
        .filter(project -> project.getPathWithNamespace().equals(projectPathWithNamespace))
        .findFirst()
        .orElseThrow(() -> new RuntimeException("Could not find project: %s".formatted(projectPathWithNamespace)));
  }
}
