package com.croqueto.backend.api.logger;

import com.croqueto.backend.api.model.WebhookRequest;
import com.croqueto.backend.api.model.WebhookResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
class LoggerImpl implements Logger {

  private final ObjectMapper objectMapper;

  @Override
  public void request(WebhookRequest webhookRequest) {
    logAsJson("REQUEST: \n{}", webhookRequest);
  }

  @Override
  public void response(WebhookResponse webhookResponse) {
    logAsJson("RESPONSE: \n{}", webhookResponse);
  }

  private void logAsJson(String s, Object obj) {
    try {
      log.info(s, objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(obj));
    } catch (JsonProcessingException e) {
      log.warn("Could not parse object into json. Cause: {}", e.getMessage());
    }
  }


}
