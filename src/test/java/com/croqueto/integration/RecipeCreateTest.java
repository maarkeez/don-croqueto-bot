package com.croqueto.integration;

import static com.croqueto.integration.IntegrationTestFactory.croquetoApi;
import static com.croqueto.integration.IntegrationTestFactory.firstMessageOf;
import static com.croqueto.integration.IntegrationTestFactory.ingredient;
import static com.croqueto.integration.IntegrationTestFactory.newSessionId;
import static com.croqueto.integration.IntegrationTestFactory.recipeService;
import static java.util.concurrent.TimeUnit.MINUTES;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.assertj.core.api.Assertions.assertThat;
import static org.awaitility.Awaitility.await;

import com.croqueto.recipe.Recipe;
import com.google.cloud.dialogflow.v2.QueryResult;
import java.util.List;
import org.junit.jupiter.api.Test;

class RecipeCreateTest extends BaseIntegrationTest {

  @Test
  void createRecipe() {

    // Given: recipe data
    final String sessionId = newSessionId();
    final String recipeName = "Croquetas de Jamón";
    final String firstIngredient = "100 gramos de harina";
    final String recipeSteps = "Paso 1, Paso 2, Paso 3";

    // When: creating a new recipe (happy path)
    final QueryResult greetingResult = croquetoApi().intent().findIntentBySessionIdAndText(sessionId, "hola");
    final QueryResult recipeCreateResult = croquetoApi().intent().findIntentBySessionIdAndText(sessionId, "quiero crear una nueva receta");
    final QueryResult nameResult = croquetoApi().intent().findIntentBySessionIdAndText(sessionId, "la voy a llamar " + recipeName);
    final QueryResult ingredientOneResult = croquetoApi().intent().findIntentBySessionIdAndText(sessionId, firstIngredient);
    final QueryResult recipeStepsResult = croquetoApi().intent().findIntentBySessionIdAndText(sessionId, "No, nada más");
    final QueryResult summaryResult = croquetoApi().intent().findIntentBySessionIdAndText(sessionId, recipeSteps);
    final QueryResult goodbyeResult = croquetoApi().intent().findIntentBySessionIdAndText(sessionId, "Sí, está perfecto");

    // Then:
    // - All the answers are the one expected
    // - All the intents are the one expected
    assertThat(greetingResult.getAction()).isEqualTo("input.welcome");

    assertThat(recipeCreateResult.getAction()).isEqualTo("input.recipe.create");
    assertThat(firstMessageOf(recipeCreateResult)).contains("nombre");

    assertThat(nameResult.getAction()).isEqualTo("input.recipe.create.name");
    assertThat(firstMessageOf(nameResult)).containsIgnoringCase("ingredientes").containsIgnoringCase(recipeName);

    assertThat(ingredientOneResult.getAction()).isEqualTo("input.recipe.create.ingredient");
    assertThat(firstMessageOf(ingredientOneResult)).contains("más");

    assertThat(recipeStepsResult.getAction()).isEqualTo("input.recipe.create.step");
    assertThat(firstMessageOf(recipeStepsResult)).containsIgnoringCase("pasos").containsIgnoringCase(recipeName);

    assertThat(summaryResult.getAction()).isEqualTo("input.recipe.create.summary");
    assertThat(firstMessageOf(summaryResult)).contains("guardar", "harina", recipeSteps, "correcto");

    assertThat(goodbyeResult.getAction()).isEqualTo("input.recipe.create.summary.confirmation");
    assertThat(firstMessageOf(goodbyeResult)).containsIgnoringCase("adiós");


  }

  @Test
  void createRecipeDifferentIngredients() {

    // Given: different recipe data
    final String sessionId = newSessionId();
    final String recipeName = "Pollo al horno";
    final String firstIngredient = "Contramuslo de pollo";
    final String secondIngredient = "Patatas";
    final String recipeSteps = "Cortar las patatas, meter el pollo en el horno y esperar 1 hora.";

    // When: creating a new recipe
    final QueryResult greetingResult = croquetoApi().intent().findIntentBySessionIdAndText(sessionId, "hola");
    final QueryResult recipeCreateResult = croquetoApi().intent().findIntentBySessionIdAndText(sessionId, "quiero crear una nueva receta");
    final QueryResult nameResult = croquetoApi().intent().findIntentBySessionIdAndText(sessionId, "el nombre de la receta es " + recipeName);
    final QueryResult ingredientOneResult = croquetoApi().intent().findIntentBySessionIdAndText(sessionId, firstIngredient);
    final QueryResult ingredientTwoResult = croquetoApi().intent().findIntentBySessionIdAndText(sessionId, secondIngredient);
    final QueryResult recipeStepsResult = croquetoApi().intent().findIntentBySessionIdAndText(sessionId, "No, nada más");
    final QueryResult summaryResult = croquetoApi().intent().findIntentBySessionIdAndText(sessionId, recipeSteps);
    final QueryResult goodbyeResult = croquetoApi().intent().findIntentBySessionIdAndText(sessionId, "Sí, está perfecto");

    // Then:
    // - The action path is the one expected
    // - The final summary is the one expected
    assertThat(greetingResult.getAction()).isEqualTo("input.welcome");
    assertThat(recipeCreateResult.getAction()).isEqualTo("input.recipe.create");
    assertThat(nameResult.getAction()).isEqualTo("input.recipe.create.name");
    assertThat(ingredientOneResult.getAction()).isEqualTo("input.recipe.create.ingredient");
    assertThat(ingredientTwoResult.getAction()).isEqualTo("input.recipe.create.ingredient");
    assertThat(recipeStepsResult.getAction()).isEqualTo("input.recipe.create.step");

    assertThat(summaryResult.getAction()).isEqualTo("input.recipe.create.summary");
    assertThat(firstMessageOf(summaryResult))
        .containsIgnoringCase(recipeName)
        .containsIgnoringCase(firstIngredient)
        .containsIgnoringCase(secondIngredient)
        .containsIgnoringCase(recipeSteps)
        .containsIgnoringCase("¿Está todo correcto?");

    assertThat(goodbyeResult.getAction()).isEqualTo("input.recipe.create.summary.confirmation");
    assertThat(firstMessageOf(goodbyeResult)).containsIgnoringCase("adiós");


  }


  @Test
  void createRecipeNameDirectly() {

    // Given: a name phrase without "el nombre de la receta" at the beginning
    final String sessionId = newSessionId();
    final String recipeName = "Pollo al horno";
    final String firstIngredient = "Contramuslos de pollo";

    // When: creating a new recipe
    final QueryResult greetingResult = croquetoApi().intent().findIntentBySessionIdAndText(sessionId, "hola");
    final QueryResult recipeCreateResult = croquetoApi().intent().findIntentBySessionIdAndText(sessionId, "quiero crear una nueva receta");
    final QueryResult nameResult = croquetoApi().intent().findIntentBySessionIdAndText(sessionId, recipeName);
    final QueryResult ingredientOneResult = croquetoApi().intent().findIntentBySessionIdAndText(sessionId, firstIngredient);

    // Then:
    // - The action path is the one expected
    // - The final summary is the one expected
    assertThat(greetingResult.getAction()).isEqualTo("input.welcome");
    assertThat(recipeCreateResult.getAction()).isEqualTo("input.recipe.create");
    assertThat(nameResult.getAction()).isEqualTo("input.recipe.create.name");
    assertThat(ingredientOneResult.getAction()).isEqualTo("input.recipe.create.ingredient");

  }

  @Test
  void createRecipeAndStored() {

    // Given: a recipe with several ingredients
    final String sessionId = newSessionId();
    final String recipeName = "Pollo al horno";
    final String firstIngredient = "Contramuslos de pollo";
    final String secondIngredient = "Patatas";
    final String recipeSteps = "Cortamos las patatas en tiras, sal pimentar, y echar con el pollo en una bandeja. Después, se mete todo al horno a 200ºC";

    // When: creating a new recipe
    croquetoApi().intent().findIntentBySessionIdAndText(sessionId, "hola");
    croquetoApi().intent().findIntentBySessionIdAndText(sessionId, "quiero crear una nueva receta");
    croquetoApi().intent().findIntentBySessionIdAndText(sessionId, "el nombre de la receta es " + recipeName);
    croquetoApi().intent().findIntentBySessionIdAndText(sessionId, firstIngredient);
    croquetoApi().intent().findIntentBySessionIdAndText(sessionId, secondIngredient);
    croquetoApi().intent().findIntentBySessionIdAndText(sessionId, "No, nada más");
    croquetoApi().intent().findIntentBySessionIdAndText(sessionId, recipeSteps);
    croquetoApi().intent().findIntentBySessionIdAndText(sessionId, "Sí, está perfecto");

    // Then: the recipe must be stored
    await("Until at least one recipe is stored")
        .atMost(1, MINUTES)
        .pollInterval(3, SECONDS)
        .until(this::atLeastOneRecipeStored);

    final List<Recipe> storedRecipes = recipeService().findAll();

    final Recipe expectedRecipe = Recipe.builder()
        .name(recipeName)
        .steps(recipeSteps)
        .ingredient(ingredient(firstIngredient.toLowerCase()))
        .ingredient(ingredient(secondIngredient.toLowerCase()))
        .build();

    assertThat(storedRecipes).hasSize(1).contains(expectedRecipe);

  }

  private boolean atLeastOneRecipeStored() {
    return !recipeService().findAll().isEmpty();
  }

}
