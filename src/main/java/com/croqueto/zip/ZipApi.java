package com.croqueto.zip;

import java.nio.file.Path;

public interface ZipApi {
    Path zipDirectory(Path directoryToBeZipped);
}
