package com.croqueto.intent;

import com.google.cloud.dialogflow.v2.*;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;

@RequiredArgsConstructor
class IntentApiImpl implements IntentApi {

    @NonNull
    private final String projectId;
    @NonNull
    private final String languageCode;

    @Override
    @SneakyThrows
    public QueryResult findIntentBySessionIdAndText(@NonNull String sessionId, @NonNull String text) {

        try (final SessionsClient sessionsClient = SessionsClient.create()) {

            final SessionName session = SessionName.of(projectId, sessionId);
            final TextInput.Builder textInput = TextInput.newBuilder().setText(text).setLanguageCode(languageCode);
            final QueryInput queryInput = QueryInput.newBuilder().setText(textInput).build();
            final DetectIntentResponse response = sessionsClient.detectIntent(session, queryInput);
            
            return response.getQueryResult();

        }

    }
}
