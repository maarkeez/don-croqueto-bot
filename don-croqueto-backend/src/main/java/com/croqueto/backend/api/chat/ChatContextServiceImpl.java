package com.croqueto.backend.api.chat;

import static java.util.Optional.ofNullable;
import static lombok.AccessLevel.PRIVATE;

import com.croqueto.recipe.Recipe;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Synchronized;

@NoArgsConstructor(access = PRIVATE)
class ChatContextServiceImpl implements ChatContextService {

  private static final ChatContextServiceImpl INSTANCE = new ChatContextServiceImpl();

  private final Map<String, Recipe> chatContexts = new ConcurrentHashMap<>();

  static ChatContextServiceImpl getInstance() {
    return INSTANCE;
  }

  @Override
  @Synchronized
  public void insert(String sessionId, Recipe recipe) {
    chatContexts.put(sessionId, recipe);
  }

  @Override
  @Synchronized
  public Recipe findBySessionId(@NonNull String sessionId) {
    return ofNullable(chatContexts.get(sessionId))
        .orElseGet(() -> Recipe.builder().build());
  }

  @Override
  @Synchronized
  public Map<String, Recipe> findAll() {
    return chatContexts;
  }

  @Override
  public void delete(String sessionId) {
    chatContexts.remove(sessionId);
  }

}
