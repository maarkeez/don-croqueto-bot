package com.croqueto.backend.api.model;

import static lombok.AccessLevel.PRIVATE;

import java.util.List;
import java.util.Map;
import lombok.NoArgsConstructor;
import lombok.Singular;
import lombok.Value;
import lombok.experimental.SuperBuilder;

@Value
@SuperBuilder(toBuilder = true)
@NoArgsConstructor(force = true, access = PRIVATE)
public class WebhookRequest {

  String responseId;
  String session;
  QueryResult queryResult;

  @Value
  @SuperBuilder(toBuilder = true)
  @NoArgsConstructor(force = true, access = PRIVATE)
  public static class QueryResult {
    String action;
    String queryText;
    @Singular
    Map<String, Object> parameters;
    boolean allRequiredParamsPresent;
    String fulfillmentText;
    @Singular
    List<FulfillmentMessage> fulfillmentMessages;
    @Singular
    List<OutputContext> outputContexts;
    Intent intent;
    double intentDetectionConfidence;
  }


  @Value
  @SuperBuilder(toBuilder = true)
  @NoArgsConstructor(force = true, access = PRIVATE)
  public static class OutputContext {
    String name;
    int lifespanCount;
    @Singular
    Map<String, Object> parameters;
  }

  @Value
  @SuperBuilder(toBuilder = true)
  @NoArgsConstructor(force = true, access = PRIVATE)
  public static class Intent {

    String name;
    String displayName;
  }

}
