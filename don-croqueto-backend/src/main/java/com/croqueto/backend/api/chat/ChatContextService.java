package com.croqueto.backend.api.chat;

import com.croqueto.recipe.Recipe;
import java.util.Map;

public interface ChatContextService {

  void insert(String sessionId, Recipe recipe);

  Recipe findBySessionId(String sessionId);

  Map<String, Recipe> findAll();

  void delete(String sessionId);

}
