package com.croqueto.recipe;

import java.util.List;

public interface RecipeService {

  void insert(Recipe recipe);

  List<Recipe> findAll();

  void deleteAll();

}
