package com.croqueto.intent;

import lombok.NoArgsConstructor;

import static lombok.AccessLevel.PRIVATE;

@NoArgsConstructor(access = PRIVATE)
public class IntentModule {

    private static final IntentModule INSTANCE = new IntentModule();
    private static final String SPANISH_LANGUAGE_CODE = "es-ES";

    public static IntentModule intentModule() {
        return INSTANCE;
    }

    public IntentApi intentApi(String projectId) {
        return new IntentApiImpl(projectId, SPANISH_LANGUAGE_CODE);
    }

}
