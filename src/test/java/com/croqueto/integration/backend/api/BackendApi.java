package com.croqueto.integration.backend.api;

import static feign.Logger.Level.FULL;
import static lombok.AccessLevel.PRIVATE;

import feign.Feign;
import feign.gson.GsonDecoder;
import feign.gson.GsonEncoder;
import feign.okhttp.OkHttpClient;
import feign.slf4j.Slf4jLogger;
import java.net.URL;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@NoArgsConstructor( access = PRIVATE )
public class BackendApi {
  
  private static final BackendApi INSTANCE = new BackendApi();
  
  public static BackendApi backendApi () {
    return INSTANCE;
  }
  
  public ActuatorApi actuator ( @NonNull URL baseUrl ) {
    return Feign.builder()
                .client(new OkHttpClient())
                .encoder(new GsonEncoder())
                .decoder(new GsonDecoder())
                .logger(new Slf4jLogger(ActuatorApi.class))
                .logLevel(FULL)
                .target(ActuatorApi.class, baseUrl.toString());
  }
  
}
