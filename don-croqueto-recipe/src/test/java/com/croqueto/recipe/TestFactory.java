package com.croqueto.recipe;

import static lombok.AccessLevel.PRIVATE;

import java.net.URL;
import java.util.Optional;
import java.util.function.Predicate;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;

@NoArgsConstructor(access = PRIVATE)
public class TestFactory {

  private static final String GITLAB_TOKEN = getenv("GITLAB_TOKEN");
  private static final String RECIPES_FILE_BRANCH = getenv("RECIPES_FILE_BRANCH");

  private static String getenv(String key) {
    return Optional.ofNullable(System.getenv(key))
        .filter(Predicate.not(String::isEmpty))
        .orElseThrow(() -> new RuntimeException("Missing environment variable: %s".formatted(key)));
  }

  @SneakyThrows
  public static URL url(String url) {
    return new URL(url);
  }

  static GitLabCredentials gitlabCredentials() {
    return GitLabCredentials.builder()
        .baseUrl(url("https://gitlab.com"))
        .accessToken(GITLAB_TOKEN)
        .build();
  }

  static RecipeConfigData recipeConfigData() {
    return RecipeConfigData.builder()
        .projectNamespace("maarkeez")
        .projectName("don-croqueto-bot-recipes")
        .fileRef(RECIPES_FILE_BRANCH)
        .filePath("recipes.json")
        .build();
  }
}
