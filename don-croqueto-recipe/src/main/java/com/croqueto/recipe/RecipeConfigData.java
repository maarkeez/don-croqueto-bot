package com.croqueto.recipe;

import lombok.NonNull;
import lombok.Value;
import lombok.experimental.SuperBuilder;

@Value
@SuperBuilder(toBuilder = true)
public class RecipeConfigData {
  @NonNull String projectNamespace;
  @NonNull String projectName;
  @NonNull String filePath;
  @NonNull String fileRef;
}
