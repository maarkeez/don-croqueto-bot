package com.croqueto.recipe;

import static lombok.AccessLevel.PRIVATE;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.gitlab4j.api.GitLabApi;

@NoArgsConstructor(access = PRIVATE)
public class RecipeModule {

  private static final RecipeModule INSTANCE = new RecipeModule();

  public static RecipeModule recipeModule() {
    return INSTANCE;
  }

  public RecipeService recipeService(@NonNull GitLabCredentials gitLabCredentials, @NonNull RecipeConfigData recipeConfigData) {
    return new RecipeServiceImpl(
        new ObjectMapper(),
        new GitLabApi(gitLabCredentials.getBaseUrl().toString(), gitLabCredentials.getAccessToken()),
        recipeConfigData
    );
  }
}
