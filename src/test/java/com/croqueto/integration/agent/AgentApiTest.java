package com.croqueto.integration.agent;

import com.croqueto.agent.AgentApi;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import lombok.Value;
import lombok.experimental.SuperBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.net.URL;
import java.nio.file.Path;

import static com.croqueto.agent.AgentModule.agentModule;
import static java.nio.file.Files.copy;
import static java.nio.file.Files.createTempDirectory;
import static java.nio.file.Paths.get;
import static org.assertj.core.api.Assertions.assertThat;

class AgentApiTest {

    private final AgentApi agentApi = agentModule().agentApi("stub-project-id");
    private final ObjectMapper objectMapper = new ObjectMapper();

    private Path givenAgentPath;
    private Path expectedAgentPath;

    @BeforeEach
    @SneakyThrows
    void setup() {

        // Copy test files into a temporal folder
        final Path resourcesPath = get("src", "test", "resources", "agent-api");
        final String givenAgentFileName = "given-agent.json";
        final String expectedAgentFileName = "expected-agent.json";

        final Path tempDirectory = createTempDirectory("agent-api");

        givenAgentPath = tempDirectory.resolve(givenAgentFileName);
        expectedAgentPath = tempDirectory.resolve(expectedAgentFileName);

        copy(resourcesPath.resolve(givenAgentFileName), givenAgentPath);
        copy(resourcesPath.resolve(expectedAgentFileName), expectedAgentPath);

    }

    @Test
    @SneakyThrows
    void changeWebhookUrl() {

        // Given: two different agent files only differing by the webhook url
        final Agent givenAgentBeforeChange = objectMapper.readValue(givenAgentPath.toFile(), Agent.class);
        final Agent expectedAgentBeforeChange = objectMapper.readValue(expectedAgentPath.toFile(), Agent.class);
        assertThat(givenAgentBeforeChange).isNotEqualTo(expectedAgentBeforeChange);

        // When: we change the webhook url of one of them
        agentApi.changeWebhookUrl(expectedAgentBeforeChange.getWebhook().getUrl(), givenAgentPath);

        // Then: both files are identical now
        final Agent givenAgentAfterChange = objectMapper.readValue(givenAgentPath.toFile(), Agent.class);
        assertThat(givenAgentAfterChange).isEqualTo(expectedAgentBeforeChange);

    }

    @Value
    @SuperBuilder(toBuilder = true)
    @NoArgsConstructor(force = true)
    private static class Agent {
        String description;
        String displayName;
        Webhook webhook;

        @Value
        @SuperBuilder(toBuilder = true)
        @NoArgsConstructor(force = true)
        private static class Webhook {
            URL url;
            String username;
        }
    }
}