package com.croqueto.backend.api;

import static com.croqueto.backend.api.chat.ChatModule.chatModule;
import static com.croqueto.backend.api.logger.LoggerModule.loggerModule;
import static com.croqueto.recipe.RecipeModule.recipeModule;

import com.croqueto.backend.api.chat.ChatContextService;
import com.croqueto.backend.api.logger.Logger;
import com.croqueto.recipe.GitLabCredentials;
import com.croqueto.recipe.RecipeConfigData;
import com.croqueto.recipe.RecipeService;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.net.URL;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CroquetoApiModule {

  private static final CroquetoApiModule INSTANCE = new CroquetoApiModule();

  public static CroquetoApiModule croquetoApiModule() {
    return INSTANCE;
  }

  @Bean
  Logger logger(ObjectMapper objectMapper) {
    return loggerModule().logger(objectMapper);
  }

  @Bean
  ChatContextService chatContextService() {
    return chatModule().chatContextService();
  }

  @Bean
  @SneakyThrows
  RecipeService recipeService(
      @Value("${gitlab.token}") String gitlabToken,
      @Value("${recipe.file.branch}") String recipeFileRef) {

    final GitLabCredentials gitlabCredentials = GitLabCredentials.builder()
        .baseUrl(new URL("https://gitlab.com"))
        .accessToken(gitlabToken)
        .build();

    final RecipeConfigData recipeConfigData = RecipeConfigData.builder()
        .projectNamespace("maarkeez")
        .projectName("don-croqueto-bot-recipes")
        .fileRef(recipeFileRef)
        .filePath("recipes.json")
        .build();

    return recipeModule().recipeService(gitlabCredentials, recipeConfigData);
  }


}
