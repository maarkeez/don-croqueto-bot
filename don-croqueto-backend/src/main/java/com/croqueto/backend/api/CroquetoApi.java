package com.croqueto.backend.api;

import com.croqueto.backend.api.model.WebhookRequest;
import com.croqueto.backend.api.model.WebhookResponse;
import com.croqueto.recipe.Recipe;
import java.util.List;
import java.util.Map;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;


@RequestMapping("croqueto/api")
public interface CroquetoApi {

  @PostMapping("event")
  ResponseEntity<WebhookResponse> agentEvent(@RequestBody WebhookRequest webhookRequest);

  @GetMapping("event")
  ResponseEntity<Map<String, Recipe>> eventFindAll();

  @GetMapping("recipe")
  ResponseEntity<List<Recipe>> recipeFindAll();

}
