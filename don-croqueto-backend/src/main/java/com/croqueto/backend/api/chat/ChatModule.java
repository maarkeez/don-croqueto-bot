package com.croqueto.backend.api.chat;

import static lombok.AccessLevel.PRIVATE;

import lombok.NoArgsConstructor;

@NoArgsConstructor(access = PRIVATE)
public class ChatModule {

  private static final ChatModule INSTANCE = new ChatModule();

  public static ChatModule chatModule() {
    return INSTANCE;
  }

  public ChatContextService chatContextService() {
    return ChatContextServiceImpl.getInstance();
  }


}
