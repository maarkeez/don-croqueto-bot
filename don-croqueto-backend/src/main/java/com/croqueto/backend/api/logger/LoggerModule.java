package com.croqueto.backend.api.logger;

import static lombok.AccessLevel.PRIVATE;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = PRIVATE)
public class LoggerModule {

  private static final LoggerModule INSTANCE = new LoggerModule();

  public static LoggerModule loggerModule() {
    return INSTANCE;
  }

  public Logger logger(ObjectMapper objectMapper) {
    return new LoggerImpl(objectMapper);
  }

}
