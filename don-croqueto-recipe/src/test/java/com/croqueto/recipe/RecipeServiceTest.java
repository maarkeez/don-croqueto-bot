package com.croqueto.recipe;

import static com.croqueto.recipe.RecipeModule.recipeModule;
import static com.croqueto.recipe.TestFactory.gitlabCredentials;
import static com.croqueto.recipe.TestFactory.recipeConfigData;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class RecipeServiceTest {

  private final GitLabCredentials gitLabCredentials = gitlabCredentials();
  private final RecipeConfigData recipeConfigData = recipeConfigData();

  private final RecipeService recipeService = recipeModule().recipeService(
      gitLabCredentials,
      recipeConfigData
  );

  @BeforeEach
  void setup() {
    recipeService.deleteAll();
  }

  @Test
  void insert() {
    final Recipe recipe = Recipe.builder()
        .name("Patatas cocidas")
        .ingredient(Recipe.Ingredient.builder().name("patatas").build())
        .steps("Cocer las patatas en una olla.")
        .build();

    recipeService.insert(recipe);
    final List<Recipe> recipes = recipeService.findAll();
    assertThat(recipes).contains(recipe);
  }

  @Test
  void findAll() {
    final List<Recipe> recipes = recipeService.findAll();
    assertThat(recipes).isEmpty();
  }
}