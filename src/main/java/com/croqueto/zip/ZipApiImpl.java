package com.croqueto.zip;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import net.lingala.zip4j.ZipFile;

import java.nio.file.Path;

import static java.nio.file.Files.createTempDirectory;
import static lombok.AccessLevel.PRIVATE;

@NoArgsConstructor(access = PRIVATE)
class ZipApiImpl implements ZipApi {

    private static final ZipApi INSTANCE = new ZipApiImpl();

    static ZipApi getInstance() {
        return INSTANCE;
    }

    @Override
    @SneakyThrows
    public Path zipDirectory(Path directoryToBeZipped) {
        final Path zipFilePath = createTempDirectory("croqueto-tmp").resolve("don-croqueto.zip");
        final ZipFile zipFile = new ZipFile(zipFilePath.toAbsolutePath().toString());
        zipFile.addFolder(directoryToBeZipped.toAbsolutePath().toFile());
        return zipFilePath;
    }

}
