# Don Croqueto Bot
[![pipeline status](https://gitlab.com/maarkeez/don-croqueto-bot/badges/master/pipeline.svg)](https://gitlab.com/maarkeez/don-croqueto-bot/-/commits/master) 

<!-- PROJECT LOGO -->
<br />
<p align="center">
  <a href="https://assets.gitlab-static.net/uploads/-/system/project/avatar/23765031/don_croqueto.jpg?width=64">
    <img src="https://assets.gitlab-static.net/uploads/-/system/project/avatar/23765031/don_croqueto.jpg?width=64" alt="Logo" width="64">
  </a>

  <h3 align="center">Don Croqueto</h3>

  <p align="center">
    <br />
    <a href="https://gitlab.com/maarkeez/don-croqueto-bot/-/wikis/home"><strong>Explore the docs »</strong></a>
    <br />
    <br />
    <a href="https://t.me/don_croqueto_bot">Don Croqueto in Telegram</a>
    ·
    <a href="https://gitlab.com/maarkeez/don-croqueto-bot/-/issues">Report Bug</a>
    ·
    <a href="https://gitlab.com/maarkeez/don-croqueto-bot/-/issues">Request Feature</a>
  </p>
</p>



<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#authors">Authors</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project

The goal of this project is to create a Telegram bot that allows you to save and read different cooking recipes.

### Built With

This is the build tool that we have chosen for the project
* [Gradle](https://gradle.org/)

<!-- GETTING STARTED -->
## Getting Started

This is an example of how you may give instructions on setting up your project locally.
To get a local copy up and running follow these simple example steps.

### Prerequisites

1. [Create a Dialogflow agent](https://cloud.google.com/dialogflow/es/docs/quick/build-agent) instance to develop
2. [Create a Google Cloud Project service account key](https://cloud.google.com/iam/docs/creating-managing-service-accounts) to be able to connect your instance
3. Configure locally the following environment variables
   ```sh
   export GOOGLE_APPLICATION_CREDENTIALS="/path/to/your/key.json"
   export GOOGLE_PROJECT_ID="your-google-cloud-project-key" 
   export BACKEND_APP_BASE_URL="https://www.your-backend-app-base-url.com"
   export RECIPES_FILE_BRANCH="your-branch-for-recipes"
   export GITLAB_TOKEN="your-gitlab-access-token"
   ```

### Installation

1. Clone the repo
   ```sh
   git clone https://gitlab.com/maarkeez/don-croqueto-bot.git
   ```
3. Build the java project
   ```sh
   gradle clean build --parallel -x test
   ```
4. Deploy the actual agent version to your instance
   ```sh
   gradle deployBot
   ```
4. Run all the test (unit and integration)
   ```sh
   gradle test --parallel
   ```


<!-- USAGE EXAMPLES -->
## Usage

The main conversations that can be achieved with Don Croqueto chatbot are documented as [automated integration tests](./src/test/java/com/croqueto/integration)

You can talk with our loved Don Croqueto at:

- Telegram
    - [Don Croqueto - Production](https://t.me/don_croqueto_bot) Instance with the latest stable released version 
    - [Don Croqueto - Test](https://t.me/don_croqueto_test_bot) Instance for testing, it is more stable than development but may not contain the latest changes
    - [Don Croqueto - Development](https://t.me/don_croqueto_dev_bot) Instance in development, it may not be stable


<!-- ROADMAP -->
## Roadmap

* **Iteration I. Bot base: Don Croqueto**

Basic chatbot without external system connections. It will be used as skeleton for the next iterations. 

* **Iteration II. Don croqueto: Telegram**

Connect the chatbot to Telegram and implement a connector that answer a random fulfillment in case a list of fulfillment it is available.

* **Iteration III. Don croqueto: recipe creation**

Add a new conversation path that allows create new recipes. In this iteration, each time we reboot the server the information will be lost.

* **Iteration IV. Don croqueto: recipe storage**

Add new server functionallity to persist all the saved recipes and be able to reboot the server without data lost.

* **Iteration V. Don croqueto: recipe read**

Add a new conversation path that allows to search for existing recipes.

<!-- CONTRIBUTING -->
## Contributing

Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any contributions you make are **greatly appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m '[feature] Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request



<!-- LICENSE.txt -->
## License

Distributed under the  GNU AGPLv3. See [LICENSE](./LICENSE.txt) for more information.



<!-- CONTACT -->
## Authors

David Márquez Delgado
