package com.croqueto.recipe;

import java.net.URL;
import lombok.NonNull;
import lombok.Value;
import lombok.experimental.SuperBuilder;

@Value
@SuperBuilder(toBuilder = true)
public class GitLabCredentials {
  @NonNull URL baseUrl;
  @NonNull String accessToken;
}
